#ifndef ROBOTFW_ROBOT_CONTROLLER_HPP
#define ROBOTFW_ROBOT_CONTROLLER_HPP

#include "arduino_controller.hpp"
#include "../includes/telemeter/telemeter.hpp"
#include <cmath>
#include "../includes/constants.hpp"

const float STEPS_PER_MM = 13.95;
const int MAX_SPEED = 600; //mm/s
const int ACCEL = 250; //mm/s²
const int BUFFER_SIZE = 50; // Enough to reach max speeds, plus security
const int ELT_MILLIS = 50; //ms
const float DIST_BETWEEN_BLUE = 143.5;
const float DIST_BETWEEN_RED  = 144.5;

const int STOP_ON_DISTANCE = 15; //cm
const int START_TO_SLOW = 40; //cm 

//For the microruptors
const char FRONT = 1;
const char REAR = -1;

typedef struct {
	float x, y, theta;
} RobotPos;

class RobotController {
private:
	ArduinoController arduino;
	Telemeter telemeter_front, telemeter_rear;
	float dist_between; //Distance between the wheels
	int micro_front, micro_rear; //Stores the indexes in micro_state array that corresponds to front/rear micros
	float left_step_distance, right_step_distance; //Distance of a step (LEFT/RIGHT allows to correct the trajectory by increasing one)
	float alpha; //Correction factor for the steppers (alpha = ratio of right wheel diameter and left wheel diameter)

	volatile float x, y, theta; //Current pos
	volatile bool _started, color, ignore_telemeter, enabled;
	volatile bool tirette, micro_state[2];

public:
	RobotController();
	~RobotController();

	void update_status(bool update_started); //Get the status of arduino Mega2560
	bool started(); //Check wether the robot is started or not
	bool set_side(); //Define side (color) and according parameters
	bool current_color(); //Return current side / color
	void enable(); //Enable servos and steppers
	void disable(); //Disable them

	//Return the indexes in the micro_state array returned by arduino that corresponds to front / rear microruptors
	int micro_front();
	int micro_rear();

	float dist_between(); //Return the defined distance between the wheels (only useful because RED != BLUE)

	RobotPos current_pos() //Return robot current position (stored and updated when moving)
	void initialize_pos(RobotPos pos) //Initialize robot positon

	void start_telemeters(); //Start telemeters, must be called at init
	void stop_telemeters(); //Stop telemeters
	//Define wether or not we want to listen to telemeters
	void set_ignore_telemeter();
	void unset_ignore_telemeter();

	//Move arms
	void arms_bottom();
	void arms_top();
	void push_candle(int which);

	//Activate ventifleur
	void enable_ventifleur() 
	void disable_ventifleur() 
	
	//Move
	int max_speed_sonar(int maxSpeedStraight, int sign); //Max speed function of what telemeters returns
	void move(float straight, float turn, int micro_to_watch = -1); //Move effectively
	void avoid(int direction);
};

#endif /* ROBOTFW_ROBOT_CONTROLLER_HPP */
