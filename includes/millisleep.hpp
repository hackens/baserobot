#ifndef ROBOTFW_MILLISLEEP_HPP
#define ROBOTFW_MILLISLEEP_HPP

#include <ctime>

const int ONEMILLI = 1000*1000;

//Sleep for N milliseconds

inline void millisleep(void)
{
    struct timespec ts = { 0, ONEMILLI };
    nanosleep(&ts, NULL);
}

inline void millisleep(int mils)
{
    struct timespec ts = { 0, mils * ONEMILLI };
    nanosleep(&ts, NULL);
}

#endif /* ROBOTFW_MILLISLEEP_HPP */
