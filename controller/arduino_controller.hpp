#ifndef ROBOTFW_ARDUINO_CONTROLLER_HPP
#define ROBOTFW_ARDUINO_CONTROLLER_HPP

#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <stdint.h>
#include "../includes/constants.hpp"

using namespace std;

const int NB_STEPPERS = 2;
const int WHEEL_LEFT = 1;
const int WHEEL_RIGHT = 0;

/* I2C */
const char COMMAND_ENABLE = 1;
const char COMMAND_ENGINES_STEPS = 2;
const char COMMAND_SERVOS = 3;
const char COMMAND_VENTIFLEUR = 4;

const int ARDUINO_DEFAULT_ADDRESS = 0x44;

//Union to send command over I2C to Arduino (must be the same in arduino code)
typedef union {
	struct { 
		char command;
		char data[31];
	} __attribute__((packed)) generic;
	struct { 
		char command;
		uint8_t engines[NB_STEPPERS];
		uint8_t servos;
	} __attribute__((packed)) enable;
	struct {
		char command;
		int16_t nbSteps[NB_STEPPERS];
		uint16_t millis;
	} __attribute__((packed)) enginesSteps;
	struct {
		char command;
		uint8_t angle[NB_SERVOS];
	} __attribute__((packed)) servos;
	struct {
		char command;
		char enable;
	} __attribute__((packed)) ventifleur;
} __attribute__((packed)) command_t;

//Union to receive arduino status (idem)
typedef struct {
	int8_t color, tirette, queueEmpty, micro1_state, micro2_state;
} __attribute__((packed)) status_t;

#define ARDUINO_TTY "/dev/i2c-4"

struct ArduinoStatus {
	bool color : 1;
	bool tirette : 1;
	bool queueEmpty : 1;
	bool micro_state[2];
};

class ArduinoController {
	private:
		int fd;
		int defaultDir[NB_STEPPERS];
		uint8_t arms[NB_SERVOS];
		pthread_mutex_t i2c_lock;

		inline ssize_t emit (command_t cmd, bool keep_lock = false, bool take_lock = true) //Sends the command_t cmd to Arduino via the i²C bus
		{
			status_t status;

			for (;;) {
				if(take_lock)
					pthread_mutex_lock(&i2c_lock);
				if(read(this->fd, &status, sizeof(status)) != sizeof(status)) {
					if(take_lock)
						pthread_mutex_unlock(&i2c_lock);
					continue;
				}
				else {
					if (!status.queueEmpty) {
						if(take_lock)
							pthread_mutex_unlock(&i2c_lock);
						usleep(40000);
						continue;
					}
					else {
						ssize_t ret = write(this->fd, &cmd, sizeof(cmd));
						if (!keep_lock)
							pthread_mutex_unlock(&i2c_lock);
						return ret;
					}
				}
			}
		}

		inline ssize_t emit_enable (uint8_t engines[NB_STEPPERS], uint8_t servos) //Send enable command
		{
			int i;
			command_t cmd;

			cmd.enable.command = COMMAND_ENABLE;

			for (i = 0; i < NB_STEPPERS; i++)
				cmd.enable.engines[i] = engines[i];
			cmd.enable.servos = servos;

			return this->emit(cmd);
		}

		inline ssize_t emit_servo (uint8_t angle[NB_SERVOS]) //Sends the command for the servos
		{
			int i;
			command_t cmd;

			cmd.servos.command = COMMAND_SERVOS;

			for (i = 0; i < NB_SERVOS; i++) {
				cmd.servos.angle[i] = angle[i];
			}

			return this->emit(cmd);
		}

		inline ssize_t emit_steps (int16_t nbSteps[NB_STEPPERS], uint16_t millis) //Sends the command for the steppers
		{
			int i;
			command_t cmd;

			cmd.enginesSteps.command = COMMAND_ENGINES_STEPS;

			for (i = 0; i < NB_STEPPERS; i++) {
				cmd.enginesSteps.nbSteps[i] = this->defaultDir[i] * nbSteps[i];
			}
			cmd.enginesSteps.millis = millis;

			return this->emit(cmd);
		}

		inline ssize_t emit_ventifleur (char enable) //Sends the command to enable / disable the ventifleur
		{
			command_t cmd;

			cmd.ventifleur.command = COMMAND_VENTIFLEUR;
			cmd.ventifleur.enable = enable;
			//Emit and keep the lock	
			return this->emit(cmd, true, true);
		}

	public:
		ArduinoController (void)
		{
			pthread_mutex_init(&i2c_lock, NULL);
			defaultDir[0] = -1; //To adapt according to wiring of steppers
			defaultDir[1] = -1;

			arms[0] = SERVOS_BOTTOM; //init the servos in bottom pos
			arms[1] = SERVOS_BOTTOM;

			int addr = ARDUINO_DEFAULT_ADDRESS; /* The I2C address */

			if ((this->fd = open(ARDUINO_TTY, O_RDWR)) < 0)
				exit(1);

			if (ioctl(this->fd, I2C_SLAVE, addr) < 0)
				exit(1);

			// Sleep for 1 second to let arduino settle down
			sleep(1);
		}

		ArduinoController(int address)
		{
			pthread_mutex_init(&i2c_lock, NULL);
			defaultDir[0] = -1;
			defaultDir[1] = -1; 

			arms[SERVO_HAUT] = SERVOS_BOTTOM;
			arms[SERVO_BAS] = SERVOS_BOTTOM;

			int addr = address; /* The I2C address */

			if ((this->fd = open(ARDUINO_TTY, O_RDWR)) < 0)
				exit(1);

			if (ioctl(this->fd, I2C_SLAVE, addr) < 0)
				exit(1);

			// Sleep for 1 second to let arduino settle down
			sleep(1);
		}

		~ArduinoController (void)
		{
			if (this->fd != 0)
				close(this->fd);
		}

		inline bool get_status (ArduinoStatus &status) //Retrieve arduino status
		{
			status_t st;

			if (read(fd, &st, sizeof(st)) != sizeof(st))
				return false;

			status.tirette = st.tirette != 0;
			status.color = st.color != 0;
			status.queueEmpty = st.queueEmpty != 0;
			status.micro_state[0] = st.micro1_state == 0;
			status.micro_state[1] = st.micro2_state == 0;

			return true;
		}

		inline void enable () //Enable servos and steppers on the robot
		{
			int i;
			uint8_t engines[NB_STEPPERS];
			uint8_t servos;

			for (i = 0; i < NB_STEPPERS; i++)
				engines[i] = 1;
			servos = 1;

			this->arms[SERVO_HAUT] = SERVOS_BOTTOM;
			this->arms[SERVO_BAS] = SERVOS_BOTTOM;

			this->emit_enable(engines, servos);
		}

		inline void disable () //Disable servos and steppers
		{
			int i;
			uint8_t engines[NB_STEPPERS];
			uint8_t servos;

			for (i = 0; i < NB_STEPPERS; i++)
				engines[i] = 0;
			servos = 0;

			this->emit_enable(engines, servos);
		}

		//Steppers
		inline void do_steps (int16_t nbSteps[NB_STEPPERS], uint16_t millis)
		{
			this->emit_steps(nbSteps, millis);
		}


		//Servos
		inline void move_arms (int pos_arms[NB_SERVOS])
		{
			this->arms[SERVO_HAUT] = pos_arms[SERVO_HAUT];
			this->arms[SERVO_BAS] = pos_arms[SERVO_BAS];
			this->emit_servo(this->arms);
		}

		//Ventifleur
		inline void ventifleur(char enable)
		{
			emit_ventifleur(enable);
		}
};

#endif /* ROBOTFW_ARDUINO_CONTROLLER_HPP */
