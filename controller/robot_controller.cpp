#include "robot_controller.hpp"
#include "../includes/millisleep.hpp"
#include <iostream>

RobotController::RobotController() {
	x=0;
	y=0;
	theta=0;
	_started= false;
	color=false;
	ignore_telemeter=false;
	enabled=false;
	tirette=true;
	micro_front=0;
	micro_rear=1;
	micro_state[0] = false;
	micro_state[1] = false;
	LEFT_STEP_DISTANCE = M_PI * 73.025 / 3200;
	RIGHT_STEP_DISTANCE = M_PI * 73.025 / 3200; 
	alpha = 0.987; 
}

RobotController::~RobotController() {
	if(this->enabled) arduino.disable();
}

//General / status functions
//==========================
void RobotController::update_status (bool update_started) {
	ArduinoStatus stat;
	if (this->arduino.get_status(stat)) {
		this->color = stat.color;
		this->tirette = stat.tirette;
		this->micro_state[0] = stat.micro_state[0];
		this->micro_state[1] = stat.micro_state[1];

		if (update_started)
			this->_started = this->_started || stat.tirette;
	}
	else
	{
		std::cout << "erreur" << std::endl;
	}
}

void RobotController::enable () {
	this->enabled = true;
	this->arduino.enable();
}

void RobotController::disable () {
	this->enabled = false;
	this->arduino.disable();
}

bool RobotController::set_side() {
	this->update_status(false);
	if(this->color == BLUE) {
		this->micro_front = 0;
		this->micro_rear = 1;
		this->telemeter_front.setAddress(115);
		this->telemeter_rear.setAddress(112);
		this->RIGHT_STEP_DISTANCE = 71.199 * M_PI / 3200;
		this->dist_between = DIST_BETWEEN_BLUE;
	}
	else {
		this->micro_front = 1;
		this->micro_rear = 0;
		this->telemeter_front.setAddress(112);
		this->telemeter_rear.setAddress(115);
		this->RIGHT_STEP_DISTANCE = 71.199 * M_PI / 3200;
		this->dist_between = DIST_BETWEEN_RED;
	}

	return this->color;
}

bool RobotController::started () {
	return this->_started;
}

bool RobotController::current_color() {
	return this->color;
}

RobotPos RobotController::current_pos () {
	RobotPos pos;
	pos.x = this->x;
	pos.y = this->y;
	pos.theta = this->theta;
	return pos;
}

void RobotController::initialize_pos (RobotPos pos) {
	this->x = pos.x;
	this->y = pos.y;
	this->theta = pos.theta;
}

int micro_rear() {
	return this->micro_rear;
}

int micro_front() {
	return this->micro_front;
}

float dist_between() {
	return this->dist_between;
}

//Functions for telemeters
//========================
RobotController::start_telemeters() {
	telemeter_front.start(); 
	telemeter_rear.start();
}

RobotController::stop_telemeters() {
	telemeter_front.stop();
	telemeter_rear.stop();
}

RobotController::set_ignore_telemeter() {
	this->ignore_telemeter = true;
}

RobotController::unset_ignore_telemeter() {
	this->ignore_telemeter = false;
}

//Functions for servos
//====================
void RobotController::arms_bottom() { 
	if(this->started()) { 
		int pos_arms[2] = {SERVOS_BOTTOM, SERVOS_BOTTOM};
		this->arduino.move_arms(pos_arms);
	} 
}
void RobotController::arms_top() { 
	if(this->started()) { 
		int pos_arms[2] = {SERVOS_TOP, SERVOS_TOP}; 
		this->arduino.move_arms(pos_arms);
	}
}
void RobotController::push_candle(int which) {
	if(this->started()) {
		int pos_arms[2];

		if(which == BOTH_SERVOS || which == SERVO_HAUT)
			pos_arms[SERVO_HAUT] = SERVOS_PUSH;
		else
			pos_arms[SERVO_HAUT] = SERVOS_TOP;

		if(which == BOTH_SERVOS || which == SERVO_BAS)
			pos_arms[SERVO_BAS] = SERVOS_PUSH;
		else
			pos_arms[SERVO_BAS] = SERVOS_TOP;

		this->arduino.move_arms(pos_arms);
	}
}

//Functions for ventifleur
//========================
void RobotController::enable_ventifleur() { 
	if (this->started()) 
		this->arduino.ventifleur(1); 
}
void RobotController::disable_ventifleur() { 
	if (this->started()) 
		this->arduino.ventifleur(0); 
}

//Functions for steppers
//======================

float sinc(float t) {
	if(fabs(t) < 0.001) return 1;
	else return sin(t)/t;
}

int RobotController::max_speed_sonar(int maxSpeedStraight, int direction) {
	int reading, ret;
	
	if(ignore_telemeter)
	{
		ret = maxSpeedStraight;
	}
	else
	{
		//Check front or rear telemeter according to direction
		if(direction > 0)
			reading = telemeter_front.get_distance();
		else
			reading = telemeter_rear.get_distance();

		ret = (reading-STOP_ON_DISTANCE) * maxSpeedStraight / (START_TO_SLOW - STOP_ON_DISTANCE;)
		
		if(ret > maxSpeedStraight)
			ret = maxSpeedStraight;

		if(ret < 0)
			ret = 0;
	}
	return ret;
}

// This function drives the robot to the wanted pos. It generates a speed trapeze automatically.
// Arguments are :
// straight = distance to make straight forward for the robot (i.e. length of the arc)
// dTheta = Angle that we want the robot to turn around it's axis


// Cf man file for explanations on calculation of the new pos from the parameters
// Examples :
// * To go straight for L (in mm), use move(L; 0);
// * To make the robot turn on itself on an angle theta (in rad), use move(0, theta)
// * To follow a circle line of radius R (in mm) and portion of angle theta (in rad), use 
//							move(R*THETA, THETA)

// micro_to_watch is a parameter that indicate the index of the micro in the micro_state array that you want to watch
// If specified, the robot will go for the specified distance and will halt if the micro is pressed. It can be easily switched to stop in unpressed microruptor. If the microruptor isn't pressed before the end of the movement, the robot will stop at the end of the end of the movement.

//NOTES :
//=======
// *** Tested only with default max_speed. There may be problems with others values (cf small robot)

void RobotController::move(float straight, float dTheta, int micro_to_watch) { //straight, turn : mm
	if(straight == 0 && dTheta == 0) //Nothing to do
		return;

	int direction = 1;

	if(straight < 0) { //Then, change direction (to switch telemeters and avoiding system)
		direction = -1;
	}

	float turn = dTheta * (DIST_BETWEEN/2); //Difference of distance between the two wheels
	
	float l = straight*sinc(dTheta/2); //Straight distance between starting and ending pos
	
	//Update pos
	x += cos(theta+dTheta/2)*l;
	y += sin(theta+dTheta/2)*l;
	theta += dTheta;

	float tmp = straight;

	//Correction (made on the right wheel == make it slightly smaller for this code to compensate some defects in the construction)
	//Alpha is the ratio of the effective diameters of the wheels. These results are just the result of a matrix calculation when taking alpha into account
	if(this->current_color() == BLUE) {
		straight = ((1+alpha) * straight + (alpha - 1) * turn) / 2;
		turn = ((alpha-1) * tmp + (alpha + 1) * turn) / 2;
	}
	else { //(this->current_color() == RED)
		straight = ((1+alpha) * straight - (alpha - 1) * turn) / 2; //change the sign of coeff on antidiagonal
		turn = (-(alpha-1) * tmp + (alpha + 1) * turn) / 2;
	}

	const float totmvt = fabs(straight) + fabs(turn);
	
	const float curAccelStraight = //steps/elt/elt 
		straight/totmvt*ACCEL*(1.f*STEPS_PER_MM*ELT_MILLIS*ELT_MILLIS/1000000);
	
	const float curAccelTurn = turn/totmvt*ACCEL*(1.f*STEPS_PER_MM*ELT_MILLIS*ELT_MILLIS/1000000);
	
	const float curMaxSpeedStraight = //steps/elt
		straight/totmvt*MAX_SPEED*(1.f*STEPS_PER_MM*ELT_MILLIS/1000);
	
	const float curMaxSpeedTurn =
		turn/totmvt*MAX_SPEED*(1.f*STEPS_PER_MM*ELT_MILLIS/1000);

	const float accelRight = curAccelStraight + curAccelTurn;
	const float accelLeft = curAccelStraight - curAccelTurn;

	//Precompute some steps to gain time when sending them
	//This step is used if code is run on Arduino (low performance to compute floating values in real time).
	//It's also useful when disminishing speed after telemeter detection
	long stepsLeft[BUFFER_SIZE], stepsRight[BUFFER_SIZE];
	int buffPos;

	//We bufferize the acceleration (we drive the motors using a speed trapeze
	for(buffPos = 0; buffPos < BUFFER_SIZE; buffPos++) {
		long coeff = (long)buffPos*(buffPos+1)/2;
		stepsLeft[buffPos] = round(accelLeft*coeff);
		stepsRight[buffPos] = round(accelRight*coeff);
		if(buffPos > 0)
			if(labs(stepsLeft[buffPos] + stepsRight[buffPos] - stepsLeft[buffPos - 1] - stepsRight[buffPos - 1]) > fabs(curMaxSpeedStraight * 2) || labs(stepsLeft[buffPos] - stepsRight[buffPos] - stepsLeft[buffPos - 1] + stepsRight[buffPos - 1]) > fabs(curMaxSpeedTurn * 2)) {
				//If we reached the maximum speed, just define new steps according to it and break
				stepsRight[buffPos] = stepsRight[buffPos - 1] + curMaxSpeedStraight + curMaxSpeedTurn;
				stepsLeft[buffPos] = stepsLeft[buffPos - 1] + curMaxSpeedStraight - curMaxSpeedTurn;
				buffPos++;
				break;
			}
	}

	long stepsRightRest = (straight+turn)*STEPS_PER_MM;
	long stepsLeftRest = (straight-turn)*STEPS_PER_MM;
	int i = 0;
	while(labs(stepsRightRest) > 0 || labs(stepsLeftRest) > 0) {
		if(micro_to_watch >= 0 && micro_to_watch < NB_SERVOS) { //If we want to stop on micro signal
			this->update_status(false);
			if(this->micro_state[micro_to_watch])
			{
				break;
			}
		}

		int ms = max_speed_sonar(MAX_SPEED, direction)*(1.f*STEPS_PER_MM*ELT_MILLIS/1000);

		if(0 == ms) { // Basic avoiding system
			avoid(direction);
		}

		int newi = i+1 < buffPos ? i+1 : i;
		//Search in the buffer for a possible value, allowed by constraints (such as max speed defined by sonar)
		for(; newi >= 1; newi--)
			if(labs(stepsLeft[newi] - stepsLeft[newi-1] + stepsRight[newi] - stepsRight[newi-1]) <= 2*ms && labs(stepsLeft[newi]) <= labs(stepsLeftRest) && labs(stepsRight[newi]) <= labs(stepsRightRest))
				break;
		i = newi;

		int16_t nbSteps[2];
		if(i > 0) {
			nbSteps[0] = stepsLeft[i] - stepsLeft[i-1];
			nbSteps[1] = stepsRight[i] - stepsRight[i-1];
		} else if(labs(stepsLeftRest + stepsRightRest) <= 2*ms) {
			nbSteps[0] = stepsLeftRest;
			nbSteps[1] = stepsRightRest;
		} else if(ms != 0) {
			if(labs(stepsLeft[0]) < labs(stepsLeftRest))
				nbSteps[0] = stepsLeft[0];
			else
				nbSteps[0] = stepsLeftRest;
			if(labs(stepsRight[1]) < labs(stepsRightRest))
				nbSteps[1] = stepsRight[1];
			else
				nbSteps[1] = stepsRightRest;
		} else {
			continue;
		}
		stepsLeftRest -= nbSteps[0];
		stepsRightRest -= nbSteps[1];

		arduino.do_steps(nbSteps, TIME_UNIT);
	}
}

//Basic avoiding system : go back for 20cm and then try to go on again
// TODO : Improve it
// = Go back exactly instead (ie including turn)
// = When moving back, don't stop after moving 20cm straight
void RobotController::avoid(int direction) {
	if(this->ignore_telemeter) //Activate telemeters during this particular move as there may be troubles with the other robot
		this->unset_ignore_telemeter();

	move(direction*-200, 0);
	millisleep(500);
	move(direction*200, 0);

	if(this->ignore_telemeter)
		this->set_ignore_telemeter();
}
