#include <pthread.h>
#include <signal.h>
#include "../controller/robot_controller.hpp"
#include "../includes/millisleep.hpp"
#include <cmath>
#define COLOR BLUE
#define MAX_TIME 90
#define count_start_thresh 50
//Distances are in mm
//Angles are in degrees

const int DEGTORAD = M_PI / 180;

int boot_time()
{
	struct timespec t;
	clock_gettime(CLOCK_BOOTTIME, &t);
	return (int) t.tv_sec;
}

int main()
{
	//Init. variable
	RobotController robot;
	
	//Enabling the robot and the arduino
	cout << "["<<boot_time() << "]:"  << "Enabling the robot and the arduino ... " ;
	robot.enable();
	cout << "["<<boot_time() << "]:" << "enabled." << endl;

	//Wait for the trigger to be pulled off
	int count_starter = 0;
	cout << "["<<boot_time() << "]:"  << "Waiting for the trigger to be pulled off ... " << flush;
	while(count_starter < count_start_thresh)
	{
		if(robot.started())
			count_starter++;
		else
			count_starter = 0;
		robot.update_status(true);
		millisleep(10);
	}
	cout << "["<<boot_time() << "]:" << "pulled off." << endl;

	// Start the telemeter
	cout << "["<<boot_time() << "]:" << "Starting the telemeter... ";
	robot.start_telemeter(robot.address_telemeter_front);
	cout << "["<<boot_time() << "]:" << "started." << endl;

	//Make it go straight forward
	cout << "["<<boot_time() << "]:" << "Going forward." << endl;
	robot.avance(1000, 0);

	//Disabling
	cout << "["<<boot_time() << "]:" << "Disabling ...";
	robot.disable();
	cout << " disabled." << endl;

	return 1;
}
