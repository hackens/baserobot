#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include "controller/robot_controller.hpp"
#include <cmath>

int kbhit(void) {
  struct termios oldt, newt;
  int ch;
  int oldf;
 
  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);
 
  ch = getchar();
 
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);
 
  if(ch != EOF)
  {
    ungetc(ch, stdin);
    return 1;
  }
 
  return 0;
}

int readch() {
	char ch;
	if(peek_character != -1) {
		ch = peek_character;
		peek_character = -1;
		return ch;
	}
	read(0, &ch, 1);
	return ch;
}

int main() {
	int ch;
	bool exit = false;
	RobotController robot;
	
	while(!exit) {
		if(kbhit()) {
			ch = readch();
		}
		
		switch(ch) {
			case 'z':
			robot.move(100, 0);
			break;
			
			case 's':
			robot.move(-100, 0);
			break;
			
			case 'q':
			robot.move(0, 10*M_PI/180*robot.dist_between);
			break;
			
			case 'd':
			robot.move(0, -10*M_PI/180*robot.dist_between);
			break;
			
			case 'l':
			exit = true;
			break;
		}
	}
}
