#include "../controller/robot_controller.hpp"

int main()
{
	RobotController robot;
	cout << "Disabling everything ..." << endl;

	robot.disable();
	robot.disable_ventifleur();

	return 0;
}
