#include <linux/i2c-dev.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include "../millisleep.hpp"
#include "telemeter.hpp"
#include <errno.h>
#include <pthread.h>
#include <iostream>

typedef struct {
	uint8_t command;
	uint8_t value;
} __attribute__((packed)) mode_request_t;

typedef struct {
	uint8_t command;
} __attribute__((packed)) get_request_t;

typedef struct {
	uint8_t high;
	uint8_t low;
} __attribute__((packed)) answer_t;

Telemeter::Telemeter() : distance(50), stopped(false), _started(false), TELEMETER_I2C_ADDR(0x70) {
	pthread_mutex_init(&distance_mutex, NULL);
	pthread_mutex_init(&stopped_mutex, NULL);
}

Telemeter::Telemeter(int address) : distance(50), stopped(false), _started(false), TELEMETER_I2C_ADDR(address) {
	pthread_mutex_init(&distance_mutex, NULL);
	pthread_mutex_init(&stopped_mutex, NULL);
}

Telemeter::~Telemeter() {}

void set_address(int address) {
	this->TELEMETER_I2C_ADDR = address;
}


void Telemeter::start() {
	if(_started) {
		std::cerr << "Error ! 2 telemeters thread launched" << std::endl;
		return;
	}
	pthread_create (&distance_thread, NULL, &RunDistanceThread, this);
	this->_started = true;
}

void* Telemeter::run_distance_thread(void *data) {
	Telemeter *self = (Telemeter*) data;
	self->thread_loop();
	return 0;
}

void Telemeter::thread_loop() {
	this->file = open("/dev/i2c-4", O_RDWR);
	if(this->file < 0) {
		exit(1);
	}

	millisleep(200);

	if (ioctl(this->file, I2C_SLAVE, TELEMETER_I2C_ADDR) < 0) {
		exit(1);
	}

	millisleep(300);

	while (true)
	{
		//If we want to stop the telemeter
		pthread_mutex_lock(&stopped_mutex);
		bool stopped_tmp = stopped;
		pthread_mutex_unlock(&stopped_mutex);
		if(stopped_tmp)
			break;
		
		//Get reading
		int reading = this->refresh_distance();
		if (reading <= 0)
			continue;
		else this->set_distance(reading);
	}

	close(this->file);
}

void Telemeter::stop() {
	pthread_mutex_lock(&stopped_mutex);
	this->stopped = true;
	pthread_mutex_unlock(&stopped_mutex);
	pthread_join(distance_thread, NULL);
	this->stopped = false;
	this->_started = false;
}

int Telemeter::get_distance() {
	pthread_mutex_lock(&distance_mutex);
	int cur_distance = this->distance;
	pthread_mutex_unlock(&distance_mutex);
	return cur_distance;
}

void Telemeter::set_distance(int new_distance) {
	pthread_mutex_lock(&distance_mutex);
	this->distance = new_distance;
	pthread_mutex_unlock(&distance_mutex);
}

int Telemeter::refresh_distance() {
	mode_request_t mode_request;
	get_request_t get_request;
	answer_t answer;

	mode_request.command = 0;
	mode_request.value = 0x51;
	get_request.command = 2;

	if (write(this->file, &mode_request, sizeof(mode_request)) != sizeof(mode_request)) {
		perror("Telemeter::get_distance(): Write mode request failure :");
		return -1;
	}

	millisleep(80);

	if (write(file, &get_request, sizeof(get_request)) != sizeof(get_request))
	{
		perror("Telemeter::refresh_distance(): Write get request failure :");
		return -1;
	}
	if (read(file, &answer, sizeof(answer)) != sizeof(answer))
	{
		perror("Telemeter::refresh_distance(): Read failure :");
		return -1;
	} else {
		uint16_t reading = (((uint16_t)answer.high) << 8) + (uint16_t) answer.low;
		return reading;
	}
}
