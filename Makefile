CC=g++ -g -W -Wall -Wextra
CFLAGS=`pkg-config opencv cvblob --cflags` -c -lphtread
LDFLAGS=`pkg-config opencv cvblob --libs` -lpthread
SOURCES=main.cpp color/candle.cpp controller/robot_controller.cpp includes/telemeter/telemeter.cpp 
TEST_SOURCES=test.cpp color/candle.cpp controller/robot_controller.cpp includes/telemeter/telemeter.cpp
AVANCE_SOURCES=avance.cpp color/candle.cpp controller/robot_controller.cpp includes/telemeter/telemeter.cpp
OBJECTS=$(SOURCES:.cpp=.o)
TEST_OBJECTS=$(TEST_SOURCES:.cpp=.o)
EXECUTABLE=robot
TEST_EXECUTABLE=test


all: $(SOURCES) $(EXECUTABLE) 
		
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -rf $(OBJECTS) $(EXECUTABLE) $(TEST_OBJECTS) $(TEST_EXECUTABLE)

test: $(TEST_SOURCES) $(TEST_EXECUTABLE) 
	$(CC) $(CFLAGS) test.cpp
	$(CC) $(LDFLAGS) $(TEST_OBJECTS) -o $(TEST_EXECUTABLE)

