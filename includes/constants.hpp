#ifndef ROBOTFW_CONSTANTS_HPP
#define ROBOTFW_CONSTANTS_HPP

const int NB_SERVOS = 2;

//Possible positions for servos (here, only three possible states)
const int SERVOS_TOP = 1;
const int SERVOS_PUSH = 0;
const int SERVOS_BOTTOM = -1;

//Position of servos (needed for the indexes in arrays)
const int SERVO_HAUT = 0;
const int SERVO_BAS = 1;
//If we want to push both servos, use this
const int BOTH_SERVOS = -1;

//Define for the starting color
const bool RED = false;
const bool BLUE = true;

#endif /* ROBOTFW_CONSTANTS_HPP */
