#ifndef __TELEMETER__
#define __TELEMETER__

class Telemeter {
private:
	int file;
	int distance;
	bool stopped, _started;
	int TELEMETER_I2C_ADDR;
	pthread_mutex_t stopped_mutex;

	pthread_mutex_t distance_mutex;
	pthread_t distance_thread;

	//Called by start automatically and range in a thread
	static void* run_distance_thread(void *data);
	void thread_loop();

	void set_distance(int distance);
	int refresh_distance();


public:
	Telemeter();
	Telemeter(int address);
	~Telemeter();

	//Set a specific address for this telemeter object (default is 112)
	void set_address(int address);

	//Call start after the init of the telemeter object to start ranging
	void start();
	//Call stop whenever you want to stop ranging and at the end of the program to avoid errors when exiting th program
	void stop();
	
	//Retrieve the distance currently stored
	int get_distance();
};

#endif // __TELEMETER__
